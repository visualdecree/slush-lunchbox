# Slush Lunchbox [![Build Status](https://secure.travis-ci.org/vdecree/slush-lunchbox.png?branch=master)](https://travis-ci.org/vdecree/slush-lunchbox) [![NPM version](https://badge-me.herokuapp.com/api/npm/slush-lunchbox.png)](http://badges.enytc.com/for/npm/slush-lunchbox)

> A Gulp scaffold for all your new projects.

# Important notes
There is currently a bug in Gulp sass and Sublime Text where it causes partials to not be seen sometimes when you are saving your sass. In order to avoid this bug change the following setting in your Sublime Text settings:

`"atomic_save": true`

## Getting Started

Install `slush-lunchbox` globally:

```bash
$ npm install -g slush-lunchbox
```

### Usage

Create a new folder for your project:

```bash
$ mkdir my-slush-lunchbox
```

Run the generator from within the new folder:

```bash
$ cd my-slush-lunchbox && slush lunchbox
```

## Bundled Dependancies

Lunchbox comes with *breakpoint*, *modular-scale* and *susy* — pre-bundled and ready to go. We've also included animate.scss which by default gets included and will be output in your CSS. You can remove this from style.scss if not required.  

## Getting To Know Slush

Slush is a tool that uses Gulp for project scaffolding.

Slush does not contain anything "out of the box", except the ability to locate installed slush generators and to run them with liftoff.

To find out more about Slush, check out the [documentation](https://github.com/slushjs/slush).

## Contributing

See the [CONTRIBUTING Guidelines](https://bitbucket.org/visualdecree/slush-lunchbox/src/09d8a7ec23cffd937b77ac3bb7cb63b8a46f0460/CONTRIBUTING.md?at=master&fileviewer=file-view-default)

## Support
If you have any problem or suggestion please open an issue [here](https://bitbucket.org/visualdecree/slush-lunchbox/issues/).

## License

The MIT License

Copyright (c) 2015, Doidge

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
